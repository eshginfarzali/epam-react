/*eslint-disable*/
import React, { useState } from "react";
import "./App.css";
import {Title, Slider } from "./components";



function App() {
  const [isDarkTheme, setIsDarkTheme] = useState(false); 

  const handleClick = event => {
    setIsDarkTheme(!isDarkTheme); 
    document.body.classList.toggle( isDarkTheme ? 'dark' : 'light'); 
  };

  return (
    <div  >
      <Title/>
      <Slider  onClick={handleClick}/>
      <ul className="planets-list ">
        <li> Mercury</li>
        <li> Venus</li>
        <li> Earth</li>
        <li> Mars</li>
        <li> Jupiter</li>
        <li> Saturn</li>
        <li> Uranus</li>
        <li> Neptune</li>
      </ul>
    </div>
  )
}

export default App;