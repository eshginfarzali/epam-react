/*eslint-disable*/ 

import './slider.css'


export const Slider = ({onClick}) => {
  return (
    <div onClick={onClick}>
    <label className="switch" htmlFor="checkbox">
    <input type="checkbox" id="checkbox"   />
    <div className="slider round"></div>
    </label>
    </div>
  )
}
